package com.misiontic.buela_facil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuelaFacilApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuelaFacilApplication.class, args);
	}

}
